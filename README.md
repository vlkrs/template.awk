# template.awk

WERC template engine
http://werc.cat-v.org

Extracted from 
https://code.9front.org/hg/werc/file/644b64e3f6a5/bin/template.awk

Released into the Public Domain as per
https://code.9front.org/hg/werc/file/644b64e3f6a5/README